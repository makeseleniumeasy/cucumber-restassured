package JsonReader;


import java.io.FileReader;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;



public class JsonReadingUtils {
	
	
	
	// Load json file to parse
	public static JSONObject  loadJsonFile(String jsonFilePath) throws IOException, ParseException
	{
		JSONParser parser = new JSONParser();
		FileReader fr = new FileReader(jsonFilePath);
		return (JSONObject)parser.parse(fr);
		
	}
	
	
	public static String getValueOfAttribute(String attributeName, JSONObject object)
	{
		String name = (String) object.get(attributeName);
		return name;
	}
	
	public static JSONArray getJsonArray(String arrayName, JSONObject object)
	{
		JSONArray array = (JSONArray) object.get(arrayName);
		return array;
	}
	
	
	public static JSONObject getJsonObject(Object objectName, JSONObject object)
	{
		JSONObject array = (JSONObject) object.get(objectName);
		return array;
	}

}
