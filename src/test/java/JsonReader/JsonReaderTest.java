package JsonReader;

import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.testng.annotations.Test;

import io.restassured.internal.path.json.JSONAssertion;

public class JsonReaderTest {
	
	@Test
	public void getDirectAttribute() throws IOException, ParseException
	{
		JSONObject object = JsonReadingUtils.loadJsonFile("G:\\MakeSeleniumEasy\\RestAssured_Framework\\GETResponsedata.json");
		String idValue = JsonReadingUtils.getValueOfAttribute("id", object);		
		System.out.println("Id Value is "+ idValue);
		String dobValue = JsonReadingUtils.getValueOfAttribute("date_of_birth", object);		
		System.out.println("DOB value is "+ dobValue);
	}
	
	
	
	@Test
	public void getArrayAttribute() throws IOException, ParseException
	{
		JSONObject object = JsonReadingUtils.loadJsonFile("G:\\MakeSeleniumEasy\\RestAssured_Framework\\GETResponsedata.json");
		JSONArray identifierArray = JsonReadingUtils.getJsonArray("identifiers", object);
		
		System.out.println("Size of array "+identifierArray.size());
		
		JSONObject firstIdentifier = (JSONObject) identifierArray.get(0);
		
		JSONObject typeObject = (JSONObject) firstIdentifier.get("type");
		
		System.out.println(typeObject.get("uid"));
		System.out.println(typeObject.get("code"));
		System.out.println(typeObject.get("display"));
		System.out.println(typeObject.get("codesystem_uri"));
		
		
		System.out.println(firstIdentifier.get("value"));
		System.out.println(firstIdentifier.get("effective_from"));
		System.out.println(firstIdentifier.get("effective_to"));
		System.out.println(firstIdentifier.get("created_by"));
		
		
	}
	
	
	@Test
	public void getJosnObjectAttribute() throws IOException, ParseException
	{
		JSONObject object = JsonReadingUtils.loadJsonFile("G:\\MakeSeleniumEasy\\RestAssured_Framework\\GETResponsedata.json");
		JSONObject NameObject = JsonReadingUtils.getJsonObject("name", object);
		
		System.out.println(NameObject.get("prefix"));
		System.out.println(NameObject.get("first"));
		System.out.println(NameObject.get("middle"));
		System.out.println(NameObject.get("family"));
		
	}

}
