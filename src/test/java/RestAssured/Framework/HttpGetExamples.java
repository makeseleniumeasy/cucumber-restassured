package RestAssured.Framework;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

import org.testng.annotations.Test;

import io.restassured.http.ContentType;

public class HttpGetExamples {

	/*
	 * This tests the status code of get request. Test will pass if status code is 200.
	 */
	//@Test
	public void getEx001()
	{
		given()
		.when()
		.get("http://ergast.com/api/f1/2017/circuits.json")
		.then()
		.statusCode(200);
		
	}
	
	/*
	 * The verification part of the test does the following:

	  1. Captures the (JSON) response of the API call
	  2. Queries for all elements called circuitId using the Groovy GPath expression 
	  "MRData.CircuitTable.Circuits.circuitId"
	  3. Verifies (using the aforementioned Hamcrest matcher) that the resulting collection 
	  of circuitId elements has size 20.
	 */
	
	@Test
	public void getEx002()
	{
		given()
		.when()
		.get("http://ergast.com/api/f1/2017/circuits.json")
		.then()
		.assertThat()
        .body("MRData.xmlns",hasSize(20));
		
	}
	
	
	/*
	 * This tests the status code of get request. Test will pass if status code is 200.
	 */
	//@Test
	public void getEx003()
	{
		given()
		.when()
		.get("http://ergast.com/api/f1/2017/circuits.json")
		.then()
		.statusCode(200)
		.contentType(ContentType.JSON)
		.header("Content-Type", "application/json; charset=utf-8")
		.header("Content-Length", "4551");
		
	}
}
