package RestAssured.Framework;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import io.restassured.response.Response;
import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.given;
public class RestTest {
    public static Response doGetRequest(String endpoint) {
        RestAssured.defaultParser = Parser.JSON;
        return
                given().headers("Content-Type", ContentType.JSON, "Accept", ContentType.JSON).
                        when().get(endpoint).
                        then().contentType(ContentType.JSON).extract().response();
    }
    public static void main(String[] args) {
        Response response = doGetRequest("http://restapi.demoqa.com/utilities/books/getallbooks");
        /*List<Map<Object,Object>> jsonResponse = response.jsonPath().getList("books");
        
        
        for(Map<Object,Object> m : jsonResponse)
        {
  
        	
        	System.out.println(m.get("title"));
        } 
        }*/
        
        int length = response.jsonPath().get("books.size()");
        
        for(int i =0;i < length ;i++)
        {
        	System.out.println(response.jsonPath().get("books["+i+"].isbn"));
        }
     
    
}
}